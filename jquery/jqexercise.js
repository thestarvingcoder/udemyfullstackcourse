//select all divs and give them a purple background
$("div").css("background", "purple");

//select the divs with class "highlight" and make them 200 px wide
$("div.highlight").css("width", "200px");

//select the div with id "third" and give it an orange border
$("#third").css("border", "4px solid orange");

//bonus: select the first div only and change its font color to pin
$("div:first-of-type").css("color", "pink");