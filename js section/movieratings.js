//movie database

var movies = [
    {
        name: "Deadpool",
        viewed: true,
        rating:4
            },
            {
                name: "Avengers Infinity War",
                viewed: true,
                rating:3
            },
            {
                name: "Frozen",
                viewed: true,
                rating:0
            }
]

movies.forEach(function(movie){
    var result = "You have ";
    if(movie.viewed){
        result += "watched ";
    } else {
        result += "not seen ";
    }
    result += "\"" + movie.name + "\" - ";
    result += movie.rating + " stars";
    console.log(result)
})